﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bex_api_cs
{
    public static class Bex_config
    {
        public const string bex_uri = "..."; 
        public const string bex_ident = "...";
        public const string softwaretoken_file = "C:\\token.p12";
        public const string softwaretoken_pass = "...";
        public const bool use_utf8_encodings = true;
    }
}
