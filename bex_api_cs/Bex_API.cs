/*
 *  beA.expert BEA-API / EXPERIMENTAL
 *  ---------------------------------
 *  Demo script not intented for production
 *  Version 3.1 / 21.08.2024
 *  (c) be next GmbH (Licence: GPL-2.0 & BSD-3-Clause)
 *  https://opensource.org/licenses/GPL-2.0
 *  https://opensource.org/licenses/BSD-3-Clause
 *
 *  
 *  Dependencies: 
 *  ------------
 *  Install-Package BouncyCastle
 *  Install-Package Newtonsoft.Json
 *  Install-Package RestSharp
 * 
 *  What is missing:
 *  ---------------
 *  - bea_get_message: decrypt message with CBC
 *  - bea_init_message_draft: read the encryptedObjects and fill the msg_infos.msg_text
 *  - bea_init_message_draft: read the XJustiz and fill the msg_infos
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Runtime.InteropServices;


using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Crypto.Parameters;

using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;

// Create an alias for BouncyCastle's X509Certificate
using BcX509Certificate = Org.BouncyCastle.X509.X509Certificate;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Utilities.Collections;
using Org.BouncyCastle.X509.Store;
using System.Diagnostics;
using Org.BouncyCastle.OpenSsl;

namespace bex_api_cs
{
    public static class Bex_API
    {
        public static string send_request(string _req, string _func)
        {
            RestRequest request;
            RestClient? client;
            string result = "";

            try
            {
                client = new RestClient(Bex_config.bex_uri + _func);
                client.ConfigureWebRequest((r) =>
                {
                    r.ServicePoint.Expect100Continue = false;
                    r.KeepAlive = false;
                });

                request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddHeader("bex-ident", Bex_config.bex_ident);
                request.AddParameter("j",Convert.ToBase64String(Encoding.UTF8.GetBytes(_req)));
                IRestResponse response = client.Execute(request);


                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = response.Content;
                    Console.WriteLine(result);
                }
                else
                {
                    string error = String.Format("StatusCode = {0} - {1}", response.StatusCode, response.ErrorMessage);
                    Console.WriteLine(error);
                    throw new Exception(response.ErrorMessage, response.ErrorException);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
               client = null;
            }

            return result;
        }

        private static byte[] read_file(string f)
        {
            byte[] result;

            try
            {
                FileStream fstream = new FileStream(f, FileMode.Open, FileAccess.Read);
                long size = fstream.Length;
                result = new byte[size];

                size = fstream.Read(result, 0, (int)size);
                fstream.Close();
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }


        public static void ConvertP12ToPem(string p12Path, string pemOutPath, string p12Password)
        {
            // Read the .p12 file
            byte[] p12Data = File.ReadAllBytes(p12Path);

            // Load the .p12 data
            Pkcs12Store pkcs12Store = new Pkcs12Store(new MemoryStream(p12Data), p12Password.ToCharArray());

            // Initialize the PEM writer
            using (StreamWriter streamWriter = new StreamWriter(pemOutPath))
            {
                PemWriter pemWriter = new PemWriter(streamWriter);

                // Iterate through the aliases to find the private key and certificate
                foreach (string alias in pkcs12Store.Aliases)
                {
                    if (pkcs12Store.IsKeyEntry(alias))
                    {
                        // Get the key entry
                        AsymmetricKeyEntry keyEntry = pkcs12Store.GetKey(alias);

                        // Write the private key to the PEM file
                        pemWriter.WriteObject(keyEntry.Key);

                        // Get the certificate chain
                        foreach (X509CertificateEntry certEntry in pkcs12Store.GetCertificateChain(alias))
                        {
                            // Write each certificate to the PEM file
                            pemWriter.WriteObject(certEntry.Certificate);
                        }

                        break; // Assuming only one key pair is present
                    }
                }
            }

            Console.WriteLine($"Conversion complete. The PEM file is saved as {pemOutPath}");
        }

    
        public static string SignCmsCommandLineEdition(Dictionary<string, string> p12Struct, string toSignChallenge, string pathToOpenssl = "")
        {
            // Extract p12_struct elements
            string swToken = p12Struct.ContainsKey("sw_token") ? p12Struct["sw_token"] : "";
            string swPin = p12Struct.ContainsKey("sw_pin") ? p12Struct["sw_pin"] : "";
            string tokenB64 = p12Struct.ContainsKey("token_b64") ? p12Struct["token_b64"] : "";

            // Define temp dir
            string tempDir = Path.Combine(Path.GetTempPath(), "bex_cs_cms_signer_temp");
            if (Directory.Exists(tempDir)) Directory.Delete(tempDir, true);
            Directory.CreateDirectory(tempDir);

            // Save p12 token to temp
            string userP12File = swToken;
            if (!string.IsNullOrEmpty(tokenB64))
            {
                userP12File = Path.Combine(tempDir, "x.p12");
                byte[] tokenRaw = Convert.FromBase64String(tokenB64);
                File.WriteAllBytes(userP12File, tokenRaw);
            }

            string tmpPemFile = Path.Combine(tempDir, "x.pem");
            ConvertP12ToPem(userP12File, tmpPemFile, swPin);

            // Define signature input file
            string challengeTxtFile = Path.Combine(tempDir, "challenge.txt");
            File.WriteAllText(challengeTxtFile, toSignChallenge);

            // Define signature out file
            string signatureP7mFile = Path.Combine(tempDir, "response.p7m");

            // Define openssl executable
            string opensslExe = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "openssl.exe" : "openssl";
            if (!string.IsNullOrEmpty(pathToOpenssl)) opensslExe = Path.Combine(pathToOpenssl, opensslExe);

            // Build the command
            string cmd = $"{opensslExe} cms -sign -in {challengeTxtFile} -out {signatureP7mFile} -signer {tmpPemFile} -keyopt rsa_padding_mode:pss -keyopt rsa_pss_saltlen:32 -outform pem -md SHA256 -passin pass:{swPin}";

            // Execute the command
            ExecuteCommand(cmd);

            // Read response signed
            string signatureClean;
            using (StreamReader reader = new StreamReader(signatureP7mFile))
            {
                string signatureUgly = reader.ReadToEnd();
                signatureClean = signatureUgly.Replace("-----BEGIN CMS-----", "")
                                            .Replace("-----END CMS-----", "")
                                            .Replace("\r", "")
                                            .Replace("\n", "");
            }

            // Cleanup temp dir
            Directory.Delete(tempDir, true);

            // Return signed value
            return signatureClean;
        }

        static void ExecuteCommand(string command)
        {
            // Determine the OS platform
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                // Windows command execution
                RunProcess("cmd.exe", $"/c {command}");
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                // macOS and Linux command execution
                RunProcess("/bin/bash", $"-c \"{command}\"");
            }
            else
            {
                throw new PlatformNotSupportedException("Unsupported OS platform");
            }
        }

        static void RunProcess(string shell, string arguments)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo
            {
                FileName = shell,
                Arguments = arguments,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            using (Process process = new Process { StartInfo = processStartInfo })
            {
                process.Start();

                string output = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();

                process.WaitForExit();

                if (process.ExitCode != 0)
                {
                    throw new Exception($"Command failed with exit code {process.ExitCode}: {error}");
                }

                Console.WriteLine(output);
            }
        }

        

        public static string X509_sign_string(X509Certificate2 cert, string digestToSign)
        {
            byte[] inputBytes = Convert.FromBase64String(digestToSign);
            byte[] result;

            using (RSA rsa = cert.GetRSAPrivateKey())
                result = rsa.SignData(inputBytes, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            return Convert.ToBase64String(result);
        }

        public static string X509_decrypt_string(X509Certificate2 cert, string value)
        {
            byte[] inputBytes = Convert.FromBase64String(value);
            byte[] result;

            using (RSA rsa = cert.GetRSAPrivateKey())
                result = rsa.Decrypt(inputBytes, RSAEncryptionPadding.OaepSHA256);

            return Convert.ToBase64String(result);
        }

        public static byte[] decrypt_gcm_bytes(byte[] ciphertext, byte[] nonce, byte[] tag, byte[] key)
        {
            byte[] plaintextBytes = new byte[ciphertext.Length];

            try
            {
                var cipher = new GcmBlockCipher(new AesEngine());
                var parameters = new AeadParameters(new KeyParameter(key), tag.Length * 8, nonce);
                cipher.Init(false, parameters);
                var bcCiphertext = ciphertext.Concat(tag).ToArray();
                var offset = cipher.ProcessBytes(bcCiphertext, 0, bcCiphertext.Length, plaintextBytes, 0);
                cipher.DoFinal(plaintextBytes, offset);
            }
            catch (Exception)
            {
                throw;
            }

            return plaintextBytes;
        }

        public static string decrypt_gcm_string(byte[] ciphertext, byte[] nonce, byte[] tag, byte[] key)
        {
            byte[] plaintextBytes = decrypt_gcm_bytes(ciphertext, nonce, tag, key);
            
            if(Bex_config.use_utf8_encodings){
                return Encoding.UTF8.GetString(plaintextBytes);
            }else{
                return Encoding.GetEncoding(28591).GetString(plaintextBytes);
            }
            
        }

        public static byte[] create_nonce(int nonceLength)
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                byte[] bytes = new byte[nonceLength];
                rng.GetBytes(bytes);
                return bytes;
            }
        }


        public static (byte[] ciphertext, byte[] nonce, byte[] tag) encrypt_gcm_bytes(byte[] plaintextBytes, byte[] key)
        {
            const int nonceLength = 16; // in bytes
            const int tagLength = 16; // in bytes

            byte[] nonce = create_nonce(nonceLength);
            var tag = new byte[tagLength];

            var bcCiphertext = new byte[plaintextBytes.Length + tagLength];
            var cipher = new GcmBlockCipher(new AesEngine());
            var parameters = new AeadParameters(new KeyParameter(key), tagLength * 8, nonce);
            cipher.Init(true, parameters);

            var offset = cipher.ProcessBytes(plaintextBytes, 0, plaintextBytes.Length, bcCiphertext, 0);
            cipher.DoFinal(bcCiphertext, offset);

            // Bouncy Castle includes the authentication tag in the ciphertext
            var ciphertext = new byte[plaintextBytes.Length];
            Buffer.BlockCopy(bcCiphertext, 0, ciphertext, 0, plaintextBytes.Length);
            Buffer.BlockCopy(bcCiphertext, plaintextBytes.Length, tag, 0, tagLength);

            return (ciphertext, nonce, tag);
        }

        public static (byte[] ciphertext, byte[] nonce, byte[] tag) encrypt_gcm_string(string plaintext, byte[] key)
        {
            byte[] plaintextBytes;
            
            if(Bex_config.use_utf8_encodings){
                plaintextBytes = Encoding.UTF8.GetBytes(plaintext);
            }else{
                plaintextBytes = Encoding.GetEncoding(28591).GetBytes(plaintext);
            }

            return encrypt_gcm_bytes(plaintextBytes, key);
        }


        public static byte[] sha256_hash_bytes(byte[] bytes)
        {
            // using BC
            SHA256Managed hashstring = new SHA256Managed();
            return hashstring.ComputeHash(bytes);
        }

        public static byte[] sha256_hash_bytes_system(byte[] bytes)
        {
            // System.Security.Cryptography
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] sha256_bytes = sha256Hash.ComputeHash(bytes);
                return sha256_bytes;
            }
        }

        public static byte[] sha256_hash_string(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            //byte[] bytes = Encoding.GetEncoding(28591).GetBytes(text);
            return sha256_hash_bytes(bytes);
        }


        public static BexSession bea_login(string p12_filepath, string p12_pass) 
        {
            // load p12 certificate
            Byte[] rawData = read_file(p12_filepath);
            X509Certificate2 cert = new X509Certificate2(rawData, p12_pass, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet);

            // get certificate thumbprint
            Console.WriteLine("Thumbprint: " + cert.Thumbprint.ToLower());
            LoginStep1Request req_step1 = new LoginStep1Request();
            req_step1.thumbprint = cert.Thumbprint.ToLower();

            // create json request
            string json_step1 = JsonConvert.SerializeObject(req_step1);

            // send request step1
            string res_step1_str = send_request(json_step1, "bea_login_step1");

            // read response
            LoginStep1Response res_step1 = JsonConvert.DeserializeObject<LoginStep1Response>(res_step1_str);




            // prepare step2
            LoginStep2Request req_step2 = new LoginStep2Request();

            // sign challenges
            req_step2.challengeSigned = X509_sign_string(cert, res_step1.challengeVal);
            req_step2.validationSigned = X509_sign_string(cert, res_step1.challengeValidation);

            // get public key and convert it to base64
            string certString = Convert.ToBase64String(cert.RawData);
            certString = String.Format("-----BEGIN CERTIFICATE-----\r{0}\r\n-----END CERTIFICATE-----", certString);
            req_step2.userCert = Convert.ToBase64String(Encoding.ASCII.GetBytes(certString));

            // set tokenPAOS
            req_step2.tokenPAOS = res_step1.tokenPAOS;

            // create json request
            string json_step2 = JsonConvert.SerializeObject(req_step2);

            // send request step2
            string res_step2_str = send_request(json_step2, "bea_login_step2");

            // read response
            LoginStep2Response res_step2 = JsonConvert.DeserializeObject<LoginStep2Response>(res_step2_str);



            // prepare step3
            LoginStep3Request req_step3 = new LoginStep3Request();
            req_step3.validationKey = X509_decrypt_string(cert, res_step2.validationKey);
            string sessionKey = X509_decrypt_string(cert, res_step2.sessionKey);

            // set tokenValidation
            req_step3.tokenValidation = res_step2.tokenValidation;

            // create json request
            string json_step3 = JsonConvert.SerializeObject(req_step3);

            // send request step3
            string res_step3_str = send_request(json_step3, "bea_login_step3");

            // read response
            LoginStep3Response res_step3 = JsonConvert.DeserializeObject<LoginStep3Response>(res_step3_str);


            // create response session
            BexSession bexSession = new BexSession();
            bexSession.safeId = res_step2.safeId;
            bexSession.sessionKey = sessionKey;
            bexSession.token = res_step3.token;
            return bexSession;
        }

        public static BexSession bex_login(string p12_filepath, string p12_pass) 
        {
            // load p12 certificate
            Byte[] rawData = read_file(p12_filepath);
            X509Certificate2 cert = new X509Certificate2(rawData, p12_pass, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.PersistKeySet);

            // get certificate thumbprint
            Console.WriteLine("Thumbprint: " + cert.Thumbprint.ToLower());
            LoginStep1Request req_step1 = new LoginStep1Request();
            req_step1.thumbprint = cert.Thumbprint.ToLower();

            // create json request
            string json_step1 = JsonConvert.SerializeObject(req_step1);

            // send request step1
            string res_step1_str = send_request(json_step1, "bex_login_step1");

            // read response
            LoginStep1Response res_step1 = JsonConvert.DeserializeObject<LoginStep1Response>(res_step1_str);



            var p12Struct = new Dictionary<string, string>
            {
                { "sw_token", p12_filepath },  // Path to the P12 file
                { "sw_pin", p12_pass },  // Password for the P12 file
                { "token_b64", "" } // Or provide base64 string
            };


            // prepare step2
            LoginStep2Request req_step2 = new LoginStep2Request();

            // sign challenges
            //req_step2.challengeSigned = X509_sign_string(cert, res_step1.challengeVal);
            //req_step2.validationSigned = X509_sign_string(cert, res_step1.challengeValidation);
            //req_step2.challengeSigned = SignCmsBouncyCastle(p12Struct, res_step1.challengeVal);
            //req_step2.validationSigned = SignCmsBouncyCastle(p12Struct, res_step1.challengeValidation);
            req_step2.challengeSigned = SignCmsCommandLineEdition(p12Struct, res_step1.challengeVal);
            req_step2.validationSigned = SignCmsCommandLineEdition(p12Struct, res_step1.challengeValidation);
            
            Console.WriteLine("req_step2.challengeSigned: " + req_step2.challengeSigned);
            Console.WriteLine("req_step2.validationSigned: " + req_step2.validationSigned);
            

            // get public key and convert it to base64
            string certString = Convert.ToBase64String(cert.RawData);
            certString = String.Format("-----BEGIN CERTIFICATE-----\r{0}\r\n-----END CERTIFICATE-----", certString);
            req_step2.userCert = Convert.ToBase64String(Encoding.ASCII.GetBytes(certString));

            // set tokenPAOS
            req_step2.tokenPAOS = res_step1.tokenPAOS;

            // create json request
            string json_step2 = JsonConvert.SerializeObject(req_step2);

            // send request step2
            string res_step2_str = send_request(json_step2, "bex_login_step2");

            // read response
            LoginStep2Response res_step2 = JsonConvert.DeserializeObject<LoginStep2Response>(res_step2_str);



            // prepare step3
            LoginStep3Request req_step3 = new LoginStep3Request();
            req_step3.validationKey = X509_decrypt_string(cert, res_step2.validationKey);
            string sessionKey = X509_decrypt_string(cert, res_step2.sessionKey);

            // set tokenValidation
            req_step3.tokenValidation = res_step2.tokenValidation;

            // create json request
            string json_step3 = JsonConvert.SerializeObject(req_step3);

            // send request step3
            string res_step3_str = send_request(json_step3, "bex_login_step3");

            // read response
            LoginStep3Response res_step3 = JsonConvert.DeserializeObject<LoginStep3Response>(res_step3_str);


            // create response session
            BexSession bexSession = new BexSession();
            bexSession.safeId = res_step2.safeId;
            bexSession.sessionKey = sessionKey;
            bexSession.token = res_step3.token;
            return bexSession;
        }


        public static LogoutResponse bea_logout(BexSession bexSession)
        {
            // create request
            LogoutRequest req = new LogoutRequest();
            req.token = bexSession.token;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_logout");

            return JsonConvert.DeserializeObject<LogoutResponse>(res_str);
        }

        public static CheckSessionResponse bea_check_session(BexSession bexSession)
        {
            // create request
            CheckSessionRequest req = new CheckSessionRequest();
            req.token = bexSession.token;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_check_session");

            return JsonConvert.DeserializeObject<CheckSessionResponse>(res_str);
        }

        public static GetPostboxesResponse bea_get_postboxes(BexSession bexSession)
        {
            // create request
            GetPostboxesRequest req = new GetPostboxesRequest();
            req.token = bexSession.token;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_postboxes");

            return JsonConvert.DeserializeObject<GetPostboxesResponse>(res_str);
        }


        public static GetFolderOverviewResponseDecrypted bea_get_folderoverview(BexSession bexSession, string folderId)
        {
            // create request
            GetFolderOverviewRequest req = new GetFolderOverviewRequest();
            req.token = bexSession.token;
            req.folderId = folderId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_folderoverview");

            // read response
            GetFolderOverviewResponse folderOverview_res = JsonConvert.DeserializeObject<GetFolderOverviewResponse>(res_str);
            
            // prepare decryption
            if (bexSession.sessionKey == "") { return null; }
            GetFolderOverviewResponseDecrypted dec_folderOverview = new GetFolderOverviewResponseDecrypted();
            dec_folderOverview.token = folderOverview_res.token;
            dec_folderOverview.messages = new List<beA_folderoverview_messages_dec>();

            // decrypt each item and push it into the decrypted structure
            foreach (beA_folderoverview_messages message in folderOverview_res.messages)
            {
                beA_folderoverview_messages_dec msg_dec = new beA_folderoverview_messages_dec();
                msg_dec.addressees = message.addressees;
                msg_dec.attachments = message.attachments;
                msg_dec.checkRequired = message.checkRequired;
                msg_dec.confidential = message.confidential;
                msg_dec.deletion = message.deletion;
                msg_dec.egvpStatus = message.egvpStatus;
                msg_dec.exported = message.exported;
                msg_dec.folderId = message.folderId;
                msg_dec.messageId = message.messageId;
                msg_dec.osciSubjectType = message.osciSubjectType;
                msg_dec.received = message.received;
                msg_dec.referenceNumber = message.referenceNumber;
                msg_dec.referenceNumberJustice = message.referenceNumberJustice;
                msg_dec.sender = message.sender;
                msg_dec.sent = message.sent;
                msg_dec.symEncAlgorithm = message.symEncAlgorithm;
                msg_dec.urgent = message.urgent;
                msg_dec.zugegangen = message.zugegangen;

                //decrypt encSubject
                if ((message.encSubject.iv != "") &&
                    (message.encSubject.tag != "") &&
                    (message.encSubject.value != ""))
                {
                    byte[] key = Convert.FromBase64String(bexSession.sessionKey);
                    byte[] iv = Convert.FromBase64String(message.encSubject.iv);
                    byte[] tag = Convert.FromBase64String(message.encSubject.tag);
                    byte[] value = Convert.FromBase64String(message.encSubject.value);
                    msg_dec.decSubject = decrypt_gcm_string(value, iv, tag, key);
                }

                // append decrypted message overview
                dec_folderOverview.messages.Add(msg_dec);
            }

            return dec_folderOverview;
        }


        public static GetFolderStructureResponse bea_get_folderstructure(BexSession bexSession, string postboxSafeId) 
        {
            // create request
            GetFolderStructureRequest req = new GetFolderStructureRequest();
            req.token = bexSession.token;
            req.postboxSafeId = postboxSafeId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_folderstructure"); 
            
            return new GetFolderStructureResponse();
        }


        public static GetIdentityDataResponse bea_get_identitydata(BexSession bexSession)
        {
            // create request
            GetIdentityDataRequest req = new GetIdentityDataRequest();
            req.token = bexSession.token;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_identitydata");

            return JsonConvert.DeserializeObject<GetIdentityDataResponse>(res_str);
        }

        public static GetUsernameResponse bea_get_username(BexSession bexSession, string identitySafeId)
        {
            // create request
            GetUsernameRequest req = new GetUsernameRequest();
            req.token = bexSession.token;
            req.identitySafeId = identitySafeId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_username");

            return JsonConvert.DeserializeObject<GetUsernameResponse>(res_str);
        }


        public static GetMessageConfigResponse bea_get_messageconfig(BexSession bexSession)
        {
            // create request
            GetMessageConfigRequest req = new GetMessageConfigRequest();
            req.token = bexSession.token;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_messageconfig");

            return JsonConvert.DeserializeObject<GetMessageConfigResponse>(res_str);
        }

        public static GetAddressbookResponse bea_get_addressbook(BexSession bexSession)
        {
            // create request
            GetAddressbookRequest req = new GetAddressbookRequest();
            req.token = bexSession.token;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_addressbook");

            return JsonConvert.DeserializeObject<GetAddressbookResponse>(res_str);
        }


        public static AddAddressbookEntryResponse bea_add_addressbookentry(BexSession bexSession, string identitySafeId)
        {
            // create request
            AddAddressbookEntryRequest req = new AddAddressbookEntryRequest();
            req.token = bexSession.token;
            req.identitySafeId = identitySafeId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_add_addressbookentry");

            return JsonConvert.DeserializeObject<AddAddressbookEntryResponse>(res_str);
        }


        public static DeleteAddressbookEntryResponse bea_delete_addressbookentry(BexSession bexSession, string addressbookEntrySafeId)
        {
            // create request
            DeleteAddressbookEntryRequest req = new DeleteAddressbookEntryRequest();
            req.token = bexSession.token;
            req.addressbookEntrySafeId = addressbookEntrySafeId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_delete_addressbookentry");

            return JsonConvert.DeserializeObject<DeleteAddressbookEntryResponse>(res_str);
        }


        public static AddFolderResponse bea_add_folder(BexSession bexSession, string parentFolderId, string newFolderName)
        {
            // create request
            AddFolderRequest req = new AddFolderRequest();
            req.token = bexSession.token;
            req.parentFolderId = parentFolderId;
            req.newFolderName = newFolderName;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_add_folder");

            return JsonConvert.DeserializeObject<AddFolderResponse>(res_str);
        }


        public static RemoveFolderResponse bea_remove_folder(BexSession bexSession, string folderId)
        {
            // create request
            RemoveFolderRequest req = new RemoveFolderRequest();
            req.token = bexSession.token;
            req.folderId = folderId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_remove_folder");

            return JsonConvert.DeserializeObject<RemoveFolderResponse>(res_str);
        }



        public static MoveMessageToFolderResponse bea_move_messagetofolder(BexSession bexSession, string messageId, string folderId)
        {
            // create request
            MoveMessageToFolderRequest req = new MoveMessageToFolderRequest();
            req.token = bexSession.token;
            req.messageId = messageId;
            req.folderId = folderId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_move_messagetofolder");

            return JsonConvert.DeserializeObject<MoveMessageToFolderResponse>(res_str);
        }


        public static MoveMessageToTrashResponse bea_move_messagetotrash(BexSession bexSession, string messageId)
        {
            // create request
            MoveMessageToTrashRequest req = new MoveMessageToTrashRequest();
            req.token = bexSession.token;
            req.messageId = messageId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_move_messagetotrash");

            return JsonConvert.DeserializeObject<MoveMessageToTrashResponse>(res_str);
        }

        public static RestoreMessageFromTrashResponse bea_restore_messagefromtrash(BexSession bexSession, string messageId)
        {
            // create request
            RestoreMessageFromTrashRequest req = new RestoreMessageFromTrashRequest();
            req.token = bexSession.token;
            req.messageId = messageId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_restore_messagefromtrash");

            return JsonConvert.DeserializeObject<RestoreMessageFromTrashResponse>(res_str);
        }

        public static DeleteMessageResponse bea_delete_message(BexSession bexSession, string messageId)
        {
            // create request
            DeleteMessageRequest req = new DeleteMessageRequest();
            req.token = bexSession.token;
            req.messageId = messageId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_delete_message");

            return JsonConvert.DeserializeObject<DeleteMessageResponse>(res_str);
        }


        public static List<(string, string)> get_message_attachment_keys(string decryptedObject)
        {
            List<(string, string)> attachment_keys = new List<(string, string)>();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(decryptedObject);

            foreach (XmlNode node_lv1 in doc.DocumentElement.ChildNodes)
            {
                if(node_lv1.LocalName == "EncryptedData")
                {
                    string tmp_name = "";
                    string tmp_key = "";
                    foreach (XmlNode node_lv2 in node_lv1.ChildNodes)
                    {
                        // extract key
                        if (node_lv2.LocalName == "KeyInfo")
                        {
                            foreach (XmlNode node_lv3 in node_lv2.ChildNodes)
                            {
                                if (node_lv3.LocalName == "MgmtData")
                                {
                                    tmp_key = node_lv3.InnerText;
                                }
                            }
                        }

                        // extract attachtment name
                        if (node_lv2.LocalName == "CipherData")
                        {
                            foreach (XmlNode node_lv3 in node_lv2.ChildNodes)
                            {
                                if (node_lv3.LocalName == "CipherReference")
                                {
                                    tmp_name = node_lv3.Attributes["URI"]?.InnerText;
                                }
                            }
                        }
                    }
                    attachment_keys.Add((tmp_name.Replace("cid:", ""), tmp_key));
                }
            }

            return attachment_keys;
        }

        public static GetMessageResponseDecrypted bea_get_message(BexSession bexSession, string messageId)
        {
            // create request
            GetMessageRequest req = new GetMessageRequest();
            req.token = bexSession.token;
            req.messageId = messageId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_get_message");

            GetMessageResponse message_encrypted = JsonConvert.DeserializeObject<GetMessageResponse>(res_str);
            GetMessageResponseDecrypted message_decrypted = new GetMessageResponseDecrypted();

            //set elements in decrypted message struct
            message_decrypted.newEGVPMessage = message_encrypted.newEGVPMessage;
            message_decrypted.osciMessageId = message_encrypted.osciMessageId;
            message_decrypted.osciSubject = message_encrypted.osciSubject;
            message_decrypted.symEncAlgorithm = message_encrypted.symEncAlgorithm;
            message_decrypted.version = message_encrypted.version;
            message_decrypted.token = message_encrypted.token;

            //set metadata in decrypted struct
            message_decrypted.metaData.addressees = message_encrypted.metaData.addressees;
            message_decrypted.metaData.checkRequired = message_encrypted.metaData.checkRequired;
            message_decrypted.metaData.confidential = message_encrypted.metaData.confidential;
            message_decrypted.metaData.created = message_encrypted.metaData.created;
            message_decrypted.metaData.eebAngefordert = message_encrypted.metaData.eebAngefordert;
            message_decrypted.metaData.messageSigned = message_encrypted.metaData.messageSigned;
            message_decrypted.metaData.messageStructureType = message_encrypted.metaData.messageStructureType;
            message_decrypted.metaData.oneAttachmentSigned = message_encrypted.metaData.oneAttachmentSigned;
            message_decrypted.metaData.originatorCertificate = message_encrypted.metaData.originatorCertificate;
            message_decrypted.metaData.originatorSignatureCertificate = message_encrypted.metaData.originatorSignatureCertificate;
            message_decrypted.metaData.receptionTime = message_encrypted.metaData.receptionTime;
            message_decrypted.metaData.referenceJustice = message_encrypted.metaData.referenceJustice;
            message_decrypted.metaData.referenceNumber = message_encrypted.metaData.referenceNumber;
            message_decrypted.metaData.sender = message_encrypted.metaData.sender;
            message_decrypted.metaData.urgent = message_encrypted.metaData.urgent;
            message_decrypted.metaData.zugegangen = message_encrypted.metaData.zugegangen;

            // prepare decryption
            if (bexSession.sessionKey == "") { return null; }
            beA_encSubject encSubject = message_encrypted.metaData.subject;
            List<beA_encrypted_object> encryptedObjects = message_encrypted.encryptedObjects;
            List<beA_encrypted_attachment> encrypted_attachments = message_encrypted.attachments;
            List<(string, string)> attachment_keys = new List<(string, string)>();


            // decrypt the subject
            if ((encSubject.iv != "") && 
                (encSubject.tag != "") &&
                (encSubject.value != ""))
            {
                byte[] key = Convert.FromBase64String(bexSession.sessionKey);
                byte[] iv = Convert.FromBase64String(encSubject.iv);
                byte[] tag = Convert.FromBase64String(encSubject.tag);
                byte[] value = Convert.FromBase64String(encSubject.value);
                message_decrypted.metaData.subject = decrypt_gcm_string(value, iv, tag, key);
            }

            // decrypt encryptedObjects
            foreach (beA_encrypted_object o in encryptedObjects)
            {
                beA_decrypted_object dec_obj = new beA_decrypted_object();

                // decrypt objectKey with sessionKey
                byte[] key = Convert.FromBase64String(bexSession.sessionKey);
                byte[] iv = Convert.FromBase64String(o.encKeyInfo.encKey.iv);
                byte[] tag = Convert.FromBase64String(o.encKeyInfo.encKey.tag);
                byte[] value = Convert.FromBase64String(o.encKeyInfo.encKey.value);
                byte[] obj_key = decrypt_gcm_bytes(value, iv, tag, key);

                // decrypt encryptedObject with objectKey
                byte[] obj_iv = Convert.FromBase64String(o.enc_iv);
                byte[] obj_tag = Convert.FromBase64String(o.enc_tag);
                byte[] obj_value = Convert.FromBase64String(o.enc_data);
                dec_obj.data = decrypt_gcm_string(obj_value, obj_iv, obj_tag, obj_key);

                // get attachment keys when project_coco
                if (o.enc_name == "project_coco")
                {
                    attachment_keys = get_message_attachment_keys(dec_obj.data);
                }

                // append decrypted object to struct
                message_decrypted.decryptedObjects.Add(dec_obj);
            }

            // decrypt attachments
            foreach (beA_encrypted_attachment a in encrypted_attachments)
            {
                // get respective attachment key
                string att_key = "";
                foreach((string, string) name_key_pair in attachment_keys)
                {
                    string tmp_name = "";
                    string tmp_key = "";
                    (tmp_name, tmp_key) = name_key_pair;

                    if(tmp_name == a.reference)
                    {
                        att_key = tmp_key;
                        break;
                    }
                }

                // decrypt attachment with the respective key
                beA_decrypted_attachment dec_att = new beA_decrypted_attachment();
                byte[] key = Convert.FromBase64String(att_key);
                byte[] iv = Convert.FromBase64String(a.iv);
                byte[] tag = Convert.FromBase64String(a.tag);
                byte[] value = Convert.FromBase64String(a.data);
                dec_att.data = decrypt_gcm_bytes(value, iv, tag, key);
                
                // set other data
                dec_att.reference = a.reference;
                dec_att.type = a.type;
                dec_att.sizeKB = a.sizeKB;
                dec_att.hashValue = a.hashValue;

                // append decrypted attachment to struct
                message_decrypted.attachments.Add(dec_att);
            }

            return message_decrypted;
        }

        public static InitMessageResponseDecrypted bea_init_message(BexSession bexSession, string postboxSafeId, message_infos_struct msg_infos)
        {
            // create request
            InitMessageRequest req = new InitMessageRequest();
            req.token = bexSession.token;
            req.postboxSafeId = postboxSafeId;
            req.msg_infos = msg_infos;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_init_message");

            InitMessageResponse enc_msg_struct =  JsonConvert.DeserializeObject<InitMessageResponse>(res_str);
            InitMessageResponseDecrypted dec_msg_struct = new InitMessageResponseDecrypted();

            // decrypt the key
            byte[] key = Convert.FromBase64String(bexSession.sessionKey);
            byte[] iv = Convert.FromBase64String(enc_msg_struct.key.iv);
            byte[] tag = Convert.FromBase64String(enc_msg_struct.key.tag);
            byte[] value = Convert.FromBase64String(enc_msg_struct.key.value);
            string msg_key = Convert.ToBase64String(decrypt_gcm_bytes(value, iv, tag, key));

            // store values in decrypted struct
            dec_msg_struct.messageToken = enc_msg_struct.messageToken;
            dec_msg_struct.key = msg_key;

            return dec_msg_struct;
        }

        public static SaveSendMessageRequest bea_encrypt_message(BexSession bexSession, string postboxSafeId, message_infos_struct msg_infos, message_decrypted_data msg_att, MessageDraft messageDraft = null)
        {
            InitMessageResponseDecrypted new_message = new InitMessageResponseDecrypted();

            // init message or use draft message
            if (messageDraft == null)
            {
                new_message = bea_init_message(bexSession, postboxSafeId, msg_infos);
            }
            else
            {
                new_message.messageToken = messageDraft.messageToken;
                new_message.key = messageDraft.key;
            }

            // prepare send/save message request
            SaveSendMessageRequest req = new SaveSendMessageRequest();
            req.messageToken = new_message.messageToken;
            req.msg_infos = msg_infos;

            // prepare encryption key
            byte[] key = Convert.FromBase64String(new_message.key);

            // encrypt subject
            message_encSubject encSubject = new message_encSubject();
            byte[] ciphertext_subject;
            byte[] nonce_subject;
            byte[] tag_subject;
            (ciphertext_subject, nonce_subject, tag_subject) = encrypt_gcm_string(msg_infos.betreff, key);

            // add encSubject to send/save message request
            encSubject.iv = Convert.ToBase64String(nonce_subject);
            encSubject.tag = Convert.ToBase64String(tag_subject);
            encSubject.value = Convert.ToBase64String(ciphertext_subject);
            encSubject.key = new_message.key;
            req.encrypted_data.encSubject = encSubject;

            // encrypt attachments
            List<message_attachments> attachments = new List<message_attachments>();

            foreach (message_attachments_decrypted a in msg_att.attachments)
            {
                // encrypt attachment data
                byte[] data_to_be_encrypted = Convert.FromBase64String(a.data);
                byte[] ciphertext;
                byte[] nonce;
                byte[] tag;
                (ciphertext, nonce, tag) = encrypt_gcm_bytes(data_to_be_encrypted, key);

                // create encrypted attachment structure
                message_attachments enc_att = new message_attachments();
                enc_att.data = Convert.ToBase64String(ciphertext);
                enc_att.iv = Convert.ToBase64String(nonce);
                enc_att.tag = Convert.ToBase64String(tag);
                enc_att.key = new_message.key;
                enc_att.name = a.name;
                enc_att.sizeKB = (int)(ciphertext.Length / 1024);
                enc_att.hash = Convert.ToBase64String(sha256_hash_bytes(data_to_be_encrypted));
                enc_att.att_type = a.att_type;

                // append encrypted attachment to list
                attachments.Add(enc_att);
            }

            // add attachments to send/save message request
            req.encrypted_data.attachments = attachments;

            return req;
        }


        public static SaveMessageResponse bea_save_message(BexSession bexSession, string postboxSafeId, message_infos_struct msg_infos, message_decrypted_data msg_att, MessageDraft messageDraft = null)
        {
            // create request
            SaveSendMessageRequest req = bea_encrypt_message(bexSession, postboxSafeId, msg_infos, msg_att, messageDraft);

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_save_message");

            return JsonConvert.DeserializeObject<SaveMessageResponse>(res_str);
        }


        public static SendMessageValidationResponse bea_send_message(BexSession bexSession, string postboxSafeId, message_infos_struct msg_infos, message_decrypted_data msg_att, MessageDraft messageDraft = null)
        {
            // create request
            SaveSendMessageRequest req = bea_encrypt_message(bexSession, postboxSafeId, msg_infos, msg_att, messageDraft);

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_send_message");
            SendMessageResponse message_validations_enc = JsonConvert.DeserializeObject<SendMessageResponse>(res_str);

            return bea_send_message_validation(bexSession, message_validations_enc);
        }


        public static SendMessageValidationResponse bea_send_message_validation(BexSession bexSession, SendMessageResponse message_validations_enc)
        {
            // create request
            SendMessageValidationRequest req = new SendMessageValidationRequest();

            // prepare decrypted validations strucure
            req.validationTokenMSG = message_validations_enc.validationTokenMSG;

            // decrypt message validations
            foreach (message_validations v in message_validations_enc.validations)
            {
                byte[] key = Convert.FromBase64String(bexSession.sessionKey);
                byte[] iv = Convert.FromBase64String(v.iv);
                byte[] tag = Convert.FromBase64String(v.tag);
                byte[] value = Convert.FromBase64String(v.data);
                string dec_data = Convert.ToBase64String(decrypt_gcm_bytes(value, iv, tag, key));

                // create decrypted validation strucure
                message_validations_decrypted dec_val = new message_validations_decrypted();
                dec_val.id = v.id;
                dec_val.data = dec_data;

                // append decrypted validation to struct
                req.validations.Add(dec_val);
            }

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_send_message_validation");

            return JsonConvert.DeserializeObject<SendMessageValidationResponse>(res_str);
        }


        public static (message_infos_struct, message_decrypted_data, MessageDraft) bea_init_message_draft(BexSession bexSession, string messageId)
        {
            // create request
            InitMessageDraftRequest req = new InitMessageDraftRequest();
            req.token = bexSession.token;
            req.messageId = messageId;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_init_message_draft");

            InitMessageDraftResponse msg_draft = JsonConvert.DeserializeObject<InitMessageDraftResponse>(res_str);

            // create message info structure
            message_infos_struct msg_infos = new message_infos_struct();
            msg_infos.aktz_sender = msg_draft.msg_infos.aktz_sender;
            msg_infos.aktz_rcv = msg_draft.msg_infos.aktz_rcv;
            msg_infos.msg_text = msg_draft.msg_infos.msg_text;
            msg_infos.is_eeb = msg_draft.msg_infos.is_eeb;
            msg_infos.dringend = msg_draft.msg_infos.dringend;
            msg_infos.pruefen = msg_draft.msg_infos.pruefen;
            msg_infos.is_eeb_response = msg_draft.msg_infos.is_eeb_response;
            msg_infos.eeb_fremdid = msg_draft.msg_infos.eeb_fremdid;
            msg_infos.eeb_date = msg_draft.msg_infos.eeb_date;
            msg_infos.verfahrensgegenstand = msg_draft.msg_infos.verfahrensgegenstand;
            msg_infos.eeb_erforderlich = msg_draft.msg_infos.eeb_erforderlich;
            msg_infos.eeb_accept = msg_draft.msg_infos.eeb_accept;
            msg_infos.xj = msg_draft.msg_infos.xj;
            msg_infos.nachrichten_typ = msg_draft.msg_infos.nachrichten_typ;
            msg_infos.eeb_reject_code = msg_draft.msg_infos.eeb_reject_code;
            msg_infos.eeb_reject_grund = msg_draft.msg_infos.eeb_reject_grund;
            msg_infos.xj_version3 = msg_draft.msg_infos.xj_version3;
            msg_infos.gericht_code = msg_draft.msg_infos.gericht_code;

            // decrypt subject and add it to the message info struct
            if ((msg_draft.msg_infos.betreff.iv != "") &&
                (msg_draft.msg_infos.betreff.tag != "") &&
                (msg_draft.msg_infos.betreff.value != ""))
            {
                byte[] key = Convert.FromBase64String(bexSession.sessionKey);
                byte[] iv = Convert.FromBase64String(msg_draft.msg_infos.betreff.iv);
                byte[] tag = Convert.FromBase64String(msg_draft.msg_infos.betreff.tag);
                byte[] value = Convert.FromBase64String(msg_draft.msg_infos.betreff.value);
                msg_infos.betreff = decrypt_gcm_string(value, iv, tag, key);
            }

            // get the receivers safeIds and add it to the message info struct
            foreach (message_init_draft_receivers r in msg_draft.msg_infos.receivers)
            {
                msg_infos.receivers.Add(r.safeId);
            }


            // decrypt the encryptedObjects to get the Attachment keys
            List<beA_decrypted_object> decryptedObjects = new List<beA_decrypted_object>();
            List<(string, string)> attachment_keys = new List<(string, string)>();

            foreach (beA_encrypted_object o in msg_draft.msg_infos.encryptedObjects)
            {
                beA_decrypted_object dec_obj = new beA_decrypted_object();

                // decrypt objectKey with sessionKey
                byte[] key = Convert.FromBase64String(bexSession.sessionKey);
                byte[] iv = Convert.FromBase64String(o.encKeyInfo.encKey.iv);
                byte[] tag = Convert.FromBase64String(o.encKeyInfo.encKey.tag);
                byte[] value = Convert.FromBase64String(o.encKeyInfo.encKey.value);
                byte[] obj_key = decrypt_gcm_bytes(value, iv, tag, key);

                // decrypt encryptedObject with objectKey
                byte[] obj_iv = Convert.FromBase64String(o.enc_iv);
                byte[] obj_tag = Convert.FromBase64String(o.enc_tag);
                byte[] obj_value = Convert.FromBase64String(o.enc_data);
                dec_obj.data = decrypt_gcm_string(obj_value, obj_iv, obj_tag, obj_key);

                // get attachment keys when project_coco
                if (o.enc_name == "project_coco")
                {
                    attachment_keys = get_message_attachment_keys(dec_obj.data);
                }

                // append decrypted object to struct
                decryptedObjects.Add(dec_obj);
            }


            // get the attachments names and add it to the message info struct
            // and decrypt the attachments to add it into the msg_att struct
            message_decrypted_data msg_att = new message_decrypted_data();
            foreach (message_init_draft_attachments a in msg_draft.msg_infos.attachments)
            {
                // decrypt the attachment
                // get respective attachment key
                string att_key = "";
                foreach ((string, string) name_key_pair in attachment_keys)
                {
                    string tmp_name = "";
                    string tmp_key = "";
                    (tmp_name, tmp_key) = name_key_pair;

                    if (tmp_name == a.reference)
                    {
                        att_key = tmp_key;
                        break;
                    }
                }

                // decrypt attachment with the respective key
                message_attachments_decrypted dec_att = new message_attachments_decrypted();
                byte[] key = Convert.FromBase64String(att_key);
                byte[] iv = Convert.FromBase64String(a.iv);
                byte[] tag = Convert.FromBase64String(a.tag);
                byte[] value = Convert.FromBase64String(a.data);
                dec_att.data = Convert.ToBase64String(decrypt_gcm_bytes(value, iv, tag, key));

                // set other data
                dec_att.name = a.reference;
                dec_att.att_type = a.type;

                // append decrypted attachment to struct if not xjustiz_nachricht.xml
                if (a.reference == "xjustiz_nachricht.xml")
                {
                    // TODO: read xj and extract infos to fill msg_infos
                }
                else
                {
                    msg_att.attachments.Add(dec_att);
                    msg_infos.attachments.Add(a.reference);
                }
            }

            // decrypt the messageKey
            byte[] _key = Convert.FromBase64String(bexSession.sessionKey);
            byte[] _iv = Convert.FromBase64String(msg_draft.key.iv);
            byte[] _tag = Convert.FromBase64String(msg_draft.key.tag);
            byte[] _value = Convert.FromBase64String(msg_draft.key.value);
            string msg_key = Convert.ToBase64String(decrypt_gcm_bytes(_value, _iv, _tag, _key));

            // create messageDraft token to send/save the message later
            MessageDraft messageDraft = new MessageDraft();
            messageDraft.messageToken = msg_draft.messageToken;
            messageDraft.key = msg_key;

            return (msg_infos, msg_att, messageDraft);
        }


        public static BeaSearchResponse bea_search(BexSession bexSession,
                                                       string identitySafeId = "",
                                                       string identityStatus = "",
                                                       string identityType = "",
                                                       string identityUsername = "",
                                                       string identityFirstname = "",
                                                       string identitySurname = "",
                                                       string identityPostalcode = "",
                                                       string identityCity = "",
                                                       string identityChamberType = "",
                                                       string identityChamberMembershipId = "",
                                                       string identityOfficeName = "")
        {
            // create request
            BeaSearchRequest req = new BeaSearchRequest();
            req.token = bexSession.token;
            req.identitySafeId = identitySafeId;
            req.identityStatus = identityStatus;
            req.identityType = identityType;
            req.identityUsername = identityUsername;
            req.identityFirstname = identityFirstname;
            req.identitySurname = identitySurname;
            req.identityPostalcode = identityPostalcode;
            req.identityCity = identityCity;
            req.identityChamberType = identityChamberType;
            req.identityChamberMembershipId = identityChamberMembershipId;
            req.identityOfficeName = identityOfficeName;

            // create json request
            string json = JsonConvert.SerializeObject(req);

            // send request
            string res_str = send_request(json, "bea_search");

            return JsonConvert.DeserializeObject<BeaSearchResponse>(res_str);
        }



    }
}