/*
 *  beA.expert BEA-API / EXPERIMENTAL
 *  ---------------------------------
 *  Demo script not intented for production
 *  Version 2.0 / 21.08.2024
 *  (c) be next GmbH (Licence: GPL-2.0 & BSD-3-Clause)
 *  https://opensource.org/licenses/GPL-2.0
 *  https://opensource.org/licenses/BSD-3-Clause
 *
 *  
 *  Dependencies: 
 *  ------------
 *  Install-Package BouncyCastle
 *  Install-Package Newtonsoft.Json
 *  Install-Package RestSharp
 * 
 *  What is missing:
 *  ---------------
 *  - bea_get_message: decrypt message with CBC
 *  - bea_init_message_draft: read the encryptedObjects and fill the msg_infos.msg_text
 *  - bea_init_message_draft: read the XJustiz and fill the msg_infos
*/

using bex_api_cs;

Console.WriteLine("bex_uri: " + Bex_config.bex_uri);
Console.WriteLine("bex_ident: " + Bex_config.bex_ident);
Console.WriteLine("softwaretoken_file: " + Bex_config.softwaretoken_file);
Console.WriteLine("softwaretoken_pass: " + Bex_config.softwaretoken_pass);


// beA login
Console.WriteLine("beA login:");
BexSession bexSession = Bex_API.bea_login(Bex_config.softwaretoken_file, Bex_config.softwaretoken_pass);
//BexSession bexSession = Bex_API.bex_login(Bex_config.softwaretoken_file, Bex_config.softwaretoken_pass);

// beA check session
Console.WriteLine("beA check session:");
CheckSessionResponse check_session = Bex_API.bea_check_session(bexSession);

// beA get postboxes
Console.WriteLine("beA get postboxes:");
GetPostboxesResponse postboxes = Bex_API.bea_get_postboxes(bexSession);

// beA get folder overview
Console.WriteLine("beA get folderoverview:");
string folderId = postboxes.postboxes[0].folder[1].id;
GetFolderOverviewResponseDecrypted folderoverview = Bex_API.bea_get_folderoverview(bexSession, folderId);

// beA get folder structure
Console.WriteLine("beA get folderstructure:");
string postboxSafeId = bexSession.safeId;
GetFolderStructureResponse folderstructure = Bex_API.bea_get_folderstructure(bexSession, postboxSafeId);

// beA get identity data
Console.WriteLine("beA get identity data:");
GetIdentityDataResponse identityData = Bex_API.bea_get_identitydata(bexSession);

// beA get username
Console.WriteLine("beA get username:");
string identitySafeId = bexSession.safeId;
GetUsernameResponse username = Bex_API.bea_get_username(bexSession, identitySafeId);

// beA get message config
Console.WriteLine("beA get message config:");
GetMessageConfigResponse messageConfig = Bex_API.bea_get_messageconfig(bexSession);

// beA get addressbook
Console.WriteLine("beA get addressbook:");
GetAddressbookResponse addressbook = Bex_API.bea_get_addressbook(bexSession);

// beA add addressbook entry
Console.WriteLine("beA add addressbook entry:");
string identitySafeId_addressbook = bexSession.safeId;
AddAddressbookEntryResponse add_addressbook = Bex_API.bea_add_addressbookentry(bexSession, identitySafeId_addressbook);

// beA delete addressbook entry
Console.WriteLine("beA delete addressbook entry:");
DeleteAddressbookEntryResponse delete_addressbook = Bex_API.bea_delete_addressbookentry(bexSession, identitySafeId_addressbook);

// beA add folder
Console.WriteLine("beA add folder:");
string parentFolderId = postboxes.postboxes[0].folder[0].id;
string newFolderName = "C#_new_folder";
AddFolderResponse added_folder = Bex_API.bea_add_folder(bexSession, parentFolderId, newFolderName);

// beA move message to folder
Console.WriteLine("beA move message to folder:");
string messageId_to_move = folderoverview.messages[0].messageId;
//MoveMessageToFolderResponse move_to_folder = Bex_API.bea_move_messagetofolder(bexSession, messageId_to_move, added_folder.id);

// beA move message to trash
Console.WriteLine("beA move message to trash:");
MoveMessageToTrashResponse move_to_trash = Bex_API.bea_move_messagetotrash(bexSession, messageId_to_move);

// beA restore message from trash
Console.WriteLine("beA restore message from trash:");
RestoreMessageFromTrashResponse restore_from_trash = Bex_API.bea_restore_messagefromtrash(bexSession, messageId_to_move);

// beA delete message
Console.WriteLine("beA delete message:");
//DeleteMessageResponse deleted_message = Bex_API.bea_delete_message(bexSession, messageId_to_move);

// beA remove folder
Console.WriteLine("beA remove folder:");
RemoveFolderResponse removed_folder = Bex_API.bea_remove_folder(bexSession, added_folder.id);

// beA get message
Console.WriteLine("beA get message:");
string messageId = folderoverview.messages[0].messageId;
GetMessageResponseDecrypted get_message = Bex_API.bea_get_message(bexSession, messageId);


// beA search
Console.WriteLine("beA search:");
BeaSearchResponse search_results_1 = Bex_API.bea_search(bexSession, "", "", "", "", "", "", "70619", "Stuttgart");
BeaSearchResponse search_results_2 = Bex_API.bea_search(bexSession, "", "", "", "", "", "", "70619", "Stuttgartä");





/*
 * send, save message
 */

// create new message strucure
Console.WriteLine("create new message:");
message_infos_struct msg_infos = new message_infos_struct();
msg_infos.betreff = "C# init message";
msg_infos.aktz_sender = "C# aktz_sender";
msg_infos.aktz_rcv = "C# aktz_rcv";
msg_infos.receivers.Add("DE.Justiztest.dd380ae8-10f8-4b5f-8dce-e54b80722409.a80d");
msg_infos.attachments.Add("01_myText.txt");

// create attachment
message_attachments_decrypted msg_attachment = new message_attachments_decrypted();
msg_attachment.name = "01_myText.txt";
msg_attachment.data = "TXkgdGV4dCAx";
msg_attachment.att_type = "SCHRIFTSATZ";

// create attachments structure to be encrypted
message_decrypted_data msg_att = new message_decrypted_data();
msg_att.attachments.Add(msg_attachment);

// beA save message
Console.WriteLine("beA save message:");
SaveMessageResponse saved_message = Bex_API.bea_save_message(bexSession, postboxSafeId, msg_infos, msg_att, null);

// beA send message
Console.WriteLine("beA send message:");
SendMessageValidationResponse sent_message = Bex_API.bea_send_message(bexSession, postboxSafeId, msg_infos, msg_att, null);





/*
 * edit-draft message
 */

// init the previously created draft message
message_infos_struct msg_infos_draft;
message_decrypted_data msg_att_draft;
MessageDraft messageDraft;
(msg_infos_draft, msg_att_draft, messageDraft) = Bex_API.bea_init_message_draft(bexSession, saved_message.messageId);

// edit the message
msg_infos_draft.betreff = "C# edit draft message";
msg_infos_draft.msg_text = "My new text C#";
msg_infos_draft.attachments.Add("02_myText.txt");

// create new attachment
message_attachments_decrypted msg_new_attachment = new message_attachments_decrypted();
msg_new_attachment.name = "02_myText.txt";
msg_new_attachment.data = "TXkgdGV4dCAx";
msg_new_attachment.att_type = "";
msg_att_draft.attachments.Add(msg_new_attachment);

// save or send the message
SaveMessageResponse saved_draft_message = Bex_API.bea_save_message(bexSession, postboxSafeId, msg_infos_draft, msg_att_draft, messageDraft);
//SendMessageValidationResponse sent_draft_message = Bex_API.bea_send_message(bexSession, postboxSafeId, msg_infos_draft, msg_att_draft, messageDraft);

// beA logout
Console.WriteLine("beA logout:");
LogoutResponse logout = Bex_API.bea_logout(bexSession);
