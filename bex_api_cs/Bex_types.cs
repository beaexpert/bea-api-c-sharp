/*
 *  beA.expert BEA-API / EXPERIMENTAL
 *  ---------------------------------
 *  Demo script not intented for production
 *  Version 1.0 / 05.03.2022
 *  (c) be next GmbH (Licence: GPL-2.0 & BSD-3-Clause)
 *  https://opensource.org/licenses/GPL-2.0
 *  https://opensource.org/licenses/BSD-3-Clause
 *
 *  
 *  Dependencies: 
 *  ------------
 *  Install-Package BouncyCastle
 *  Install-Package Newtonsoft.Json
 *  Install-Package RestSharp
 * 
 *  What is missing:
 *  ---------------
 *  - bea_get_message: decrypt message with CBC
 *  - bea_init_message_draft: read the encryptedObjects and fill the msg_infos.msg_text
 *  - bea_init_message_draft: read the XJustiz and fill the msg_infos
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bex_api_cs
{
    public class BexSession
    {
        public string token = "";
        public string safeId = "";
        public string sessionKey = "";
    }

    public class LoginStep1Request
    {
        public string thumbprint = "";
    }

    public class LoginStep1Response
    {
        public string challengeVal = "";
        public string challengeValidation = "";
        public string tokenPAOS = "";
    }

    public class LoginStep2Request
    {
        public string tokenPAOS = "";
        public string userCert = "";
        public string challengeSigned = "";
        public string validationSigned = "";
    }

    public class LoginStep2Response
    {
        public string safeId = "";
        public string sessionKey = "";
        public string tokenValidation = "";
        public string validationKey = "";
    }

    public class LoginStep3Request
    {
        public string tokenValidation = "";
        public string validationKey = "";
    }

    public class LoginStep3Response
    {
        public string[] postboxes;
        public string token = "";
    }

    public class LogoutRequest
    {
        public string token = "";
    }

    public class LogoutResponse
    {
        public string info = "";
    }

    public class CheckSessionRequest
    {
        public string token = "";
    }

    public class CheckSessionResponse
    {
        public string info = "";
        public string token = "";
    }

    public class GetPostboxesRequest
    {
        public string token = "";
    }

    public class beA_folder
    {
        public string id = "";
        public string name = "";
        public string postboxSafeId = "";
        public string type = "";
        public string parentFolderId = "";
    }

    public class beA_postbox
    {
        public string displayName = "";
        public List<beA_folder> folder = new List<beA_folder>();
        public string postboxSafeId = "";
    }

    public class GetPostboxesResponse
    {
        public List<beA_postbox> postboxes = new List<beA_postbox>();
        public string token = "";
    }

    public class GetFolderOverviewRequest
    {
        public string folderId = "";
        public string token = "";
    }

    public class beA_addressee
    {
        public string chamber = "";
        public string chamberMembershipId = "";
        public string city = "";
        public string firstname = "";
        public string identityId = "";
        public string personId = "";
        public string postalcode = "";
        public string ratype = "";
        public string safeId = "";
        public string status = "";
        public string street = "";
        public string streetnumber = "";
        public string surname = "";
        public string type = "";
        public string username = "";
    }

    public class beA_encSubject
    {
        public string iv = "";
        public string tag = "";
        public string value = "";
    }

    public class beA_folderoverview_messages
    {
        public List<beA_addressee> addressees = new List<beA_addressee>();
        public string attachments = "";
        public string checkRequired = "";
        public string confidential = "";
        public string deletion = "";
        public string egvpStatus = "";
        public beA_encSubject encSubject = new beA_encSubject();
        public string exported = "";
        public string folderId = "";
        public string messageId = "";
        public string osciSubjectType = "";
        public string received = "";
        public string referenceNumber = "";
        public string referenceNumberJustice = "";
        public beA_addressee sender = new beA_addressee();
        public string sent = "";
        public string symEncAlgorithm = "";
        public string urgent = "";
        public string zugegangen = "";

    }

    public class GetFolderOverviewResponse
    {
        public List<beA_folderoverview_messages> messages = new List<beA_folderoverview_messages>();
        public string token = "";
    }

    public class beA_folderoverview_messages_dec
    {
        public List<beA_addressee> addressees = new List<beA_addressee>();
        public string attachments = "";
        public string checkRequired = "";
        public string confidential = "";
        public string deletion = "";
        public string egvpStatus = "";
        public string decSubject = "";
        public string exported = "";
        public string folderId = "";
        public string messageId = "";
        public string osciSubjectType = "";
        public string received = "";
        public string referenceNumber = "";
        public string referenceNumberJustice = "";
        public beA_addressee sender = new beA_addressee();
        public string sent = "";
        public string symEncAlgorithm = "";
        public string urgent = "";
        public string zugegangen = "";

    }

    public class GetFolderOverviewResponseDecrypted
    {
        public List<beA_folderoverview_messages_dec> messages = new List<beA_folderoverview_messages_dec>();
        public string token = "";
    }

    public class GetFolderStructureRequest
    {
        public string token = "";
        public string postboxSafeId = "";
    }

    public class GetFolderStructureResponse
    {
        public string displayName = "";
        public List<beA_folder> folders = new List<beA_folder>();
        public string postboxSafeId = "";
        public string token = "";
    }

    public class GetIdentityDataRequest
    {
        public string token = "";
    }

    public class GetIdentityDataResponse
    {
        public string chamber = "";
        public string chamberMembershipId = "";
        public string city = "";
        public string coName = "";
        public string country = "";
        public string email = "";
        public string firstname = "";
        public string formOfAdress = "";
        public string identityId = "";
        public string organization = "";
        public string organizationExtension = "";
        public string personId = "";
        public string postalcode = "";
        public string raType = "";
        public string safeId = "";
        public string status = "";
        public string street = "";
        public string streetnumber = "";
        public string surname = "";
        public string type = "";
        public string username = "";
        public string token = "";
    }

    public class GetUsernameRequest
    {
        public string token = "";
        public string identitySafeId = "";
    }

    public class GetUsernameResponse
    {
        public string userName = "";
        public string token = "";
    }

    public class GetMessageConfigRequest
    {
        public string token = "";
    }

    public class GetMessageConfigResponse
    {
        public List<string> attachmentExtensionBlacklist = new List<string>();
        public string maxAttachmentCount = "";
        public string maxAttachmentSizeInKB = "";
        public string token = "";
    }

    public class beA_addressbook_addressee
    {
        public string chamber = "";
        public string chamberMembershipId = "";
        public string city = "";
        public string coName = "";
        public string country = "";
        public string email = "";
        public string firstname = "";
        public string formOfAdress = "";
        public string identityId = "";
        public string organization = "";
        public string organizationExtension = "";
        public string personId = "";
        public string postalcode = "";
        public string raType = "";
        public string safeId = "";
        public string status = "";
        public string street = "";
        public string streetnumber = "";
        public string surname = "";
        public string type = "";
        public string username = "";
    }

    public class GetAddressbookRequest
    {
        public string token = "";
    }

    public class GetAddressbookResponse
    {
        public List<beA_addressbook_addressee> addressbook = new List<beA_addressbook_addressee>();
        public string token = "";
    }


    public class AddAddressbookEntryRequest
    {
        public string token = "";
        public string identitySafeId = "";
    }

    public class AddAddressbookEntryResponse
    {
        public string info = "";
        public string token = "";
    }

    public class DeleteAddressbookEntryRequest
    {
        public string token = "";
        public string addressbookEntrySafeId = "";
    }

    public class DeleteAddressbookEntryResponse
    {
        public string info = "";
        public string token = "";
    }


    public class AddFolderRequest
    {
        public string token = "";
        public string parentFolderId = "";
        public string newFolderName = "";
    }

    public class AddFolderResponse
    {
        public string id = "";
        public string name = "";
        public string parentFolderId = "";
        public string postboxSafeId = "";
        public string type = "";
        public string token = "";
    }

    public class RemoveFolderRequest
    {
        public string token = "";
        public string folderId = "";
    }

    public class RemoveFolderResponse
    {
        public string info = "";
        public string token = "";
    }


    public class MoveMessageToFolderRequest
    {
        public string token = "";
        public string folderId = "";
        public string messageId = "";
    }

    public class MoveMessageToFolderResponse
    {
        public string info = "";
        public string token = "";
    }


    public class MoveMessageToTrashRequest
    {
        public string token = "";
        public string messageId = "";
    }

    public class MoveMessageToTrashResponse
    {
        public string info = "";
        public string token = "";
    }

    public class RestoreMessageFromTrashRequest
    {
        public string token = "";
        public string messageId = "";
    }

    public class RestoreMessageFromTrashResponse
    {
        public string info = "";
        public string token = "";
    }


    public class DeleteMessageRequest
    {
        public string token = "";
        public string messageId = "";
    }

    public class DeleteMessageResponse
    {
        public string info = "";
        public string token = "";
    }


    public class beA_encrypted_attachment
    {
        public string data = "";
        public string hashValue = "";
        public string iv = "";
        public string key = "";
        public string reference = "";
        public string sizeKB = "";
        public string symEncAlgorithm = "";
        public string tag = "";
        public string type = "";
    }

    public class beA_encrypted_object_encKeyInfo
    {
        public string encCertificate = "";
        public beA_encrypted_object_encKey encKey = new beA_encrypted_object_encKey();
        public string safeId = "";
    }

    public class beA_encrypted_object_encKey
    {
        public string iv = "";
        public string tag = "";
        public string value = "";
    }

    public class beA_encrypted_object
    {
        public beA_encrypted_object_encKeyInfo encKeyInfo = new beA_encrypted_object_encKeyInfo();
        public string enc_data = "";
        public string enc_iv = "";
        public string enc_name = "";
        public string enc_tag = "";
    }

    public class beA_message_addressee
    {
        public string certificate = "";
        public string name = "";
        public string safeId = "";
    }


    public class beA_message_metaData
    {
        public List<beA_message_addressee> addressees = new List<beA_message_addressee>();
        public string checkRequired = "";
        public string confidential = "";
        public string created = "";
        public string eebAngefordert = "";
        public string messageSigned = "";
        public string messageStructureType = "";
        public string oneAttachmentSigned = "";
        public string originatorCertificate = "";
        public string originatorSignatureCertificate = "";
        public string receptionTime = "";
        public string referenceJustice = "";
        public string referenceNumber = "";
        public beA_message_addressee sender = new beA_message_addressee();
        public beA_encSubject subject = new beA_encSubject();
        public string urgent = "";
        public string zugegangen = "";
    }

    public class beA_message_metaData_decrypted
    {
        public List<beA_message_addressee> addressees = new List<beA_message_addressee>();
        public string checkRequired = "";
        public string confidential = "";
        public string created = "";
        public string eebAngefordert = "";
        public string messageSigned = "";
        public string messageStructureType = "";
        public string oneAttachmentSigned = "";
        public string originatorCertificate = "";
        public string originatorSignatureCertificate = "";
        public string receptionTime = "";
        public string referenceJustice = "";
        public string referenceNumber = "";
        public beA_message_addressee sender = new beA_message_addressee();
        public string subject = "";
        public string urgent = "";
        public string zugegangen = "";
    }

    public class beA_decrypted_attachment
    {
        public string reference = "";
        public byte[] data = null;
        public string type = "";
        public string sizeKB = "";
        public string hashValue = "";
    }

    public class beA_decrypted_object
    {
        public string data = "";
    }


    public class GetMessageRequest
    {
        public string token = "";
        public string messageId = "";
    }

    public class GetMessageResponse
    {
        public List<beA_encrypted_attachment> attachments = new List<beA_encrypted_attachment>();
        public List<beA_encrypted_object> encryptedObjects = new List<beA_encrypted_object>();
        public string messageId = "";
        public beA_message_metaData metaData = new beA_message_metaData();
        public string newEGVPMessage = "";
        public string osciMessageId = "";
        public string osciSubject = "";
        public string symEncAlgorithm = "";
        public string version = "";
        public string token = "";
    }


    public class GetMessageResponseDecrypted
    {
        public List<beA_decrypted_attachment> attachments = new List<beA_decrypted_attachment>();
        public List<beA_decrypted_object> decryptedObjects = new List<beA_decrypted_object>();
        public string messageId = "";
        public beA_message_metaData_decrypted metaData = new beA_message_metaData_decrypted();
        public string newEGVPMessage = "";
        public string osciMessageId = "";
        public string osciSubject = "";
        public string symEncAlgorithm = "";
        public string version = "";
        public string token = "";
    }


    public class message_infos_struct
    {
        public string betreff = "";
        public string aktz_sender = "";
        public string aktz_rcv = "";
        public string msg_text = "";
        public bool is_eeb = false;
        public bool dringend = false;
        public bool pruefen = false;
        public List<string> receivers = new List<string>();
        public List<string> attachments = new List<string>();
        public bool is_eeb_response = false;
        public string eeb_fremdid = "";
        public string eeb_date = "";
        public string verfahrensgegenstand = "";
        public bool eeb_erforderlich = false;
        public bool eeb_accept = false;
        public bool xj = false;
        public string nachrichten_typ = "ALLGEMEINE_NACHRICHT";
        public string eeb_reject_code = "";
        public string eeb_reject_grund = "";
        public bool xj_version3 = false;
        public string gericht_code = "9A0000";
    }

    public class message_key
    {
        public string iv = "";
        public string tag = "";
        public string value = "";
    }

    public class InitMessageRequest
    {
        public string token = "";
        public string postboxSafeId = "";
        public message_infos_struct msg_infos = new message_infos_struct();
    }

    public class InitMessageResponse
    {
        public string messageToken = "";
        public message_key key = new message_key();
    }

    public class InitMessageResponseDecrypted
    {
        public string messageToken = "";
        public string key = "";
    }

    public class message_decrypted_data
    {
        public List<message_attachments_decrypted> attachments = new List<message_attachments_decrypted>();
    }

    public class message_attachments_decrypted
    {
        public string name = "";
        public string data = "";
        public string att_type = "";
    }

    public class message_encrypted_data
    {
        public message_encSubject encSubject = new message_encSubject();
        public List<message_attachments> attachments = new List<message_attachments>();
    }

    public class message_encSubject
    {
        public string iv = "";
        public string tag = "";
        public string value = "";
        public string key = "";
    }

    public class message_attachments
    {
        public string name = "";
        public string iv = "";
        public string tag = "";
        public string data = "";
        public string key = "";
        public int sizeKB = 0;
        public string hash = "";
        public string att_type = "ATTACHMENT";
    }

    public class SaveSendMessageRequest
    {
        public string messageToken = "";
        public message_encrypted_data encrypted_data = new message_encrypted_data();
        public message_infos_struct? msg_infos;
    }

    public class SaveMessageResponse
    {
        public string messageId = "";
        public string token = "";
    }

    public class SendMessageResponse
    {
        public List<message_validations> validations = new List<message_validations>();
        public string validationTokenMSG = "";
    }

    public class MessageDraft
    {
        public string messageToken = "";
        public string key = "";
    }


    public class message_validations
    {
        public string id = "";
        public string data = "";
        public string iv = "";
        public string tag = "";
    }

    public class message_validations_decrypted
    {
        public string id = "";
        public string data = "";
    }

    public class SendMessageValidationRequest
    {
        public List<message_validations_decrypted> validations = new List<message_validations_decrypted>();
        public string validationTokenMSG = "";
    }

    public class SendMessageValidationResponse
    {
        public string token = "";
        public string info = "";
        public string messageId = "";
    }


    public class InitMessageDraftRequest
    {
        public string token = "";
        public string messageId = "";
    }


    public class InitMessageDraftResponse
    {
        public message_key key = new message_key();
        public string messageToken = "";
        public message_infos_init_draft_struct msg_infos = new message_infos_init_draft_struct();
    }

    public class message_init_draft_attachments
    {
        public string reference = "";
        public string iv = "";
        public string tag = "";
        public string data = "";
        public string key = "";
        public string sizeKB = "";
        public string hashValue = "";
        public string type = "ATTACHMENT";
        public string symEncAlgorithm = "";
    }

    public class message_init_draft_receivers
    {
        public string certificate = "";
        public string name = "";
        public string safeId = "";
    }


    public class message_infos_init_draft_struct
    {
        public beA_encSubject betreff = new beA_encSubject();
        public string aktz_sender = "";
        public string aktz_rcv = "";
        public string msg_text = "";
        public bool is_eeb = false;
        public bool dringend = false;
        public bool pruefen = false;
        public List<message_init_draft_receivers> receivers = new List<message_init_draft_receivers>();
        public List<message_init_draft_attachments> attachments = new List<message_init_draft_attachments>();
        public bool is_eeb_response = false;
        public string eeb_fremdid = "";
        public string eeb_date = "";
        public string verfahrensgegenstand = "";
        public bool eeb_erforderlich = false;
        public bool eeb_accept = false;
        public bool xj = false;
        public string nachrichten_typ = "ALLGEMEINE_NACHRICHT";
        public string eeb_reject_code = "";
        public string eeb_reject_grund = "";
        public bool xj_version3 = false;
        public string gericht_code = "9A0000";
        public List<beA_encrypted_object> encryptedObjects = new List<beA_encrypted_object>();
    }


    public class BeaSearchRequest
    {
        public string token = "";
        public string identitySafeId = "";
        public string identityStatus = "";
        public string identityType = "";
        public string identityUsername = "";
        public string identityFirstname = "";
        public string identitySurname = "";
        public string identityPostalcode = "";
        public string identityCity = "";
        public string identityChamberType = "";
        public string identityChamberMembershipId = "";
        public string identityOfficeName = "";
    }

    public class BeaSearchResponse
    {
        public List<BeaSearchAddressee> results = new List<BeaSearchAddressee>();
        public string token = "";
    }

    public class BeaSearchAddressee
    {
        public string city = "";
        public string identityId = "";
        public string postalcode = "";
        public string safeId = "";
        public string status = "";
        public string street = "";
        public string streetnumber = "";
        public string firstname = "";
        public string surname = "";
        public string type = "";
        public string username = "";
    }

}
